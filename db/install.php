<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WKS News Aggregator block installation.
 *
 * @package   block_wks_news_aggregator_itms
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_block_wks_news_aggregator_install() {
    global $DB, $CFG;

    // Selecting default region for blocks i.e. content.
    $users = $DB->get_records("user", array("deleted" => 0), '', "id");
    // $users = get_users(true);
    foreach ($users as $user) {
        $params = array('news_items', CONTEXT_USER, $user->id);
        $sql = "SELECT bi.*
                FROM {block_instances} bi
                JOIN {context} c ON c.id = bi.parentcontextid
                WHERE bi.blockname = ? AND c.contextlevel = ? AND c.instanceid = ?";
        $instances = $DB->get_records_sql($sql, $params);
        if (!empty($instances)) {
            foreach ($instances as $instance) {
                blocks_delete_instance($instance);
            }
        }
        $systempage = $DB->get_record('my_pages', array('userid' => $user->id, 'private' => 1));
        $page = new moodle_page();
        $page->blocks->add_region('content');
        $page->set_context(context_user::instance($user->id));
        if ($systempage) {
            $page->blocks->add_block('wks_news_aggregator', 'content', -2, false, 'my-index', $systempage->id);
        }
    }
    return true;
}