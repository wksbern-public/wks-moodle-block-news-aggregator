<?php


$observers = array(
 array(
     	'eventname'   => '\core\event\user_loggedin',
     	'callback'    => 'add_wks_news_aggregator_block',
     	'includefile' => '/blocks/wks_news_aggregator/lib.php'
 	),
);