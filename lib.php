<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library functions for overview.
 *
 * @package   block_wks_news_aggregator
 * @copyright 2018 Peter Dias
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function get_courses_dropdown($courses) {
	$text = '<div class="dropdown mb-1 mr-1">
                        <button id="ednewsitemscourses" type="button" class="btn font-weight-bold dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="Dropdown-Menü für die Gruppierung">
                        <i class="fa fa-filter" aria-hidden="true"></i>
        
                          <span data-active-item-text>'.get_string('allcourses', 'block_wks_news_aggregator').'</span>
                        </button>';
	$text .= '<div class="dropdown-menu" aria-labelledby="coursesDropdown" role="menu">';
	$text .= '<a class="dropdown-item ed-newsitem-ditem" href="javascript:void(0)" role="menuitem" data-course-id="0">'.get_string('allcourses', 'block_wks_news_aggregator').'</a>';
	foreach ($courses as $course) {
		$text .= '<a class="dropdown-item ed-newsitem-ditem" href="javascript:void(0)" role="menuitem" data-course-id="'.$course->id.'">'.$course->fullname.'</a>';
	}
	$text .= '</div></div>';
	return $text;
}
/**
 * Function to add block on login event trigger
 */
function add_wks_news_aggregator_block(\core\event\user_loggedin $event) {
    global $DB, $CFG;
    $eventdata = $event->get_data();
    $user_id = $eventdata['userid'];
    $params = array('news_items', CONTEXT_USER, $user_id);
    $sql = "SELECT bi.*
            FROM {block_instances} bi
            JOIN {context} c ON c.id = bi.parentcontextid
            WHERE bi.blockname = ? AND c.contextlevel = ? AND c.instanceid = ?";
    $instances = $DB->get_records_sql($sql, $params);
    if (!empty($instances)) {
        foreach ($instances as $instance) {
            blocks_delete_instance($instance);
        }
        $systempage = $DB->get_record('my_pages', array('userid' => $user_id, 'private' => 1));
        $page = new moodle_page();
        $page->blocks->add_region('content');
        $page->set_context(context_user::instance($user_id));
        if ($systempage) {
            $page->blocks->add_block('wks_news_aggregator', 'content', -2, false, 'my-index', $systempage->id);
        }
    }
}