<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_wks_news_aggregator', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_wks_news_aggregator
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['wks_news_aggregator:addinstance'] = 'Add a new latest announcements block';
$string['wks_news_aggregator:myaddinstance'] = 'Add a new latest announcements block to Dashboard';
$string['pluginname'] = 'News und Ankündigungen';
$string['privacy:metadata'] = 'The Campus: News Aggregator block only shows data stored in the forum and does not store data itself.';
$string['nocourseenrolled'] = 'User is not enrolled in any course.';
$string['allcourses'] = 'Alle Fächer';
$string['noforumsfound'] = 'No Forums Found';
$pages['pages'] = 'Seite wählen';