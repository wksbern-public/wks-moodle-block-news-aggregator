<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * WKS news items Plugin trait.
 *
 * @package     blocks/wks_news_aggregator
 */
namespace block_wks_news_aggregator\external;
defined('MOODLE_INTERNAL') || die();
use external_function_parameters;
use external_value;


trait get_course_forums {

    /**
     * Describes the structure of parameters for the function.
     *
     * @return external_function_parameters
     */
    public static function get_course_forums_parameters() {
        return new external_function_parameters(
            array (
                'course_id' => new external_value(PARAM_INT, 'Course ID', VALUE_DEFAULT, 0),
                'pageid' => new external_value(PARAM_INT, 'Page number', VALUE_DEFAULT, 0),
            )
        );
    }
    /**
     * Get course forum announcements
     * @param Integer $attemptid                  Attempt Id
     **/
    public static function get_course_forums($course_id, $pageid) {
        global $CFG, $USER, $PAGE, $DB;
        if ($course_id == 0) {
            $courses = enrol_get_users_courses($USER->id, true, '*');
        } else {
            $courses = array();
            $course = $DB->get_record('course', array('id' => $course_id));
            array_push($courses, $course);
        }
        $content = new \stdClass();
        $content->text = '';
        $strftimerecent = get_string('strftimerecent');
        $strmore = get_string('more', 'forum');
        $text = '';
        $allpagediscussions = array();
        $alldiscussions = array();
        $result = array();
        require_once($CFG->dirroot.'/mod/forum/lib.php');   // We'll need this
        foreach ($courses as $course) {
            // if ($course->newsitems) {   // Create a nice listing of recent postings
            if (!$forum = forum_get_course_forum($course->id, 'news')) {
                $content->text = get_string('noforumsfound', 'block_wks_news_aggregator');
                break;
            }

            $modinfo = get_fast_modinfo($course);
            if (empty($modinfo->instances['forum'][$forum->id])) {
                $content->text = get_string('noforumsfound', 'block_wks_news_aggregator');
                break;
            }
            $cm = $modinfo->instances['forum'][$forum->id];

            if (!$cm->uservisible) {
                $content->text = get_string('noforumsfound', 'block_wks_news_aggregator');
                break;
            }

            $context = \context_module::instance($cm->id);

            /// User must have perms to view discussions in that forum
            if (!has_capability('mod/forum:viewdiscussion', $context)) {
                $content->text = get_string('noforumsfound', 'block_wks_news_aggregator');
                break;
            }

            /// First work out whether we can post to this group and if so, include a link
            $groupmode    = groups_get_activity_groupmode($cm);
            $currentgroup = groups_get_activity_group($cm, true);

            // if (forum_user_can_post_discussion($forum, $currentgroup, $groupmode, $cm, $context)) {
            //     $text .= '<div class="newlink"><a href="'.$CFG->wwwroot.'/mod/forum/post.php?forum='.$forum->id.'">'.
            //               get_string('addanewtopic', 'forum').'</a>...</div>';
            // }

            /// Get all the recent discussions we're allowed to see

            // This block displays the most recent posts in a forum in
            // descending order. The call to default sort order here will use
            // that unless the discussion that post is in has a timestart set
            // in the future.
            // This sort will ignore pinned posts as we want the most recent.
            $sort = forum_get_default_sort_order(true, 'p.modified', 'd', false);
            $discussions = forum_get_discussions($cm, $sort, false, -1, -1, false, -1, 0, FORUM_POSTS_ALL_USER_GROUPS);

            $currentDate = new \DateTime();
            $before4Month = $currentDate->sub(new \DateInterval('P4M'))->getTimestamp();
            if (!empty($discussions)) {
                foreach ($discussions as $discussion) {
                    if($discussion->modified > $before4Month) {
                        array_push($alldiscussions, $discussion);
                    }
                }
            }
            // }
        }
        if (empty($alldiscussions)) {
            $text .= '<div class="mt-10">('.get_string('nonews', 'forum').')</div>';
        }

        $numMessagesPerPage = 5;

        if (!empty($alldiscussions)) {
            usort($alldiscussions, function($first, $second) {
                return $first->timemodified < $second->timemodified;
            });

            $text .= "\n<ol class='list-group list-group-dividered list-group-full mt-10 ml-20'>\n";
            $count = $pageid ? $pageid + $numMessagesPerPage * $pageid : 1;

            for ($i=($pageid*$numMessagesPerPage); $i < ($pageid*$numMessagesPerPage)+$numMessagesPerPage; $i++) {
                if (isset($alldiscussions[$i])) {
                    $alldiscussions[$i]->subject = $alldiscussions[$i]->name;
                    $courseFullname = $courses[$alldiscussions[$i]->course]->fullname;

                    // $discussion->subject = format_string($discussion->subject, true, $forum->course);
                    //$text .= '<li class="list-unstyled" style="display:flex; justify-content: space-between; border-bottom: 1px solid #e0e0e0">';
                    $text .= '<div style="display: grid;grid-template-columns: minmax(auto, 40%)  20% 20% 20%;
                                    grid-row-gap:0.6rem;
                                    grid-column-gap: 0;
                                ">';

                    if (($i+1) == $numMessagesPerPage) {
                        // KUFA: no numbers
                        //$text .= '<span style="margin-left:-1%">'.($i+1).'.&nbsp</span>';
                    } else {
                        //$text .= '<span>'.($i+1).'.&nbsp</span>';
                    }
//                    $text .= '<div>
//                                   <div><a href="'.$CFG->wwwroot.'/mod/forum/discuss.php?d='.$alldiscussions[$i]->discussion.'">'.$alldiscussions[$i]->subject.'</a></div>
//                                   <div>'. $courseFullname . '</div>
//                               </div>';
//
//                    $text .= '<div style="width:200px;">
//                                <div>' . userdate($alldiscussions[$i]->modified, $strftimerecent).'</div>
//                                <div>'.fullname($alldiscussions[$i]).'</div>
//                            </div>';
//                    $text .= '</li>';

                    $text .= '<div style="border-bottom: 1px solid #e0e0e0; padding:7px;"><a href="'.$CFG->wwwroot.'/mod/forum/discuss.php?d='.$alldiscussions[$i]->discussion.'">'.$alldiscussions[$i]->subject.'</a></div>
                              <div style="border-bottom: 1px solid #e0e0e0; padding:7px;">'. $courseFullname . '</div>
                              <div style="border-bottom: 1px solid #e0e0e0; padding:7px;">' . fullname($alldiscussions[$i]).'</div>
                              <div style="border-bottom: 1px solid #e0e0e0; padding:7px;"><div style="width:100%; text-align: right">'.userdate($alldiscussions[$i]->modified, $strftimerecent).'</div></div>';
                    $text .= '</div>';



                    $count++;
                }
            }
            $text .= "</ol>\n";
        }
        // show pagination if more than 10 announcements (discussions)
        if (!empty($alldiscussions) && count($alldiscussions) > $numMessagesPerPage) {
            $pages = ceil(count($alldiscussions)/$numMessagesPerPage);
            if ($pages > 1) {
                $text .= '<ul class="pagination pagination-sm" style="margin-top:12px;">';
                //$text .= "<li><b>".get_string('pages', 'block_wks_news_aggregator')."</b></li>";
                for($i= 1; $i <= $pages; $i++) {
                    if ($pageid == ($i-1)) {
                        $text .= '<li class="page-item page_ids" data-page_id="'.($i-1).'"><b><a class="page-link page_ids">'.$i.'</a></b></li>';
                    } else {
                        $text .= '<li class="page-item page_ids" data-page_id="'.($i-1).'"><a class="page-link page_ids">'.$i.'</a></li>';
                    }
                }
                $text .= '</ul>';
            }
        }
        $content->text .= $text;
        $result['content'] = $content->text;
        return $result;
    }
    /**
     * Describes the structure of the function return value.
     *
     * @return external_single_structure
     */
    public static function get_course_forums_returns() {
        return new \external_single_structure(
            array(
                'content' => new \external_value(PARAM_RAW, 'HTML content with forum topics')
            )
        );
    }
    public function edcompare($first, $second) {
        return $first->timemodified < $second->timemodified;
    }
}
