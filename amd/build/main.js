define(["jquery", "core/ajax"], function($, Ajax) {
    return {
        init: function() {
        	var course_id = 0;
        	$(".ed-newsitem-ditem").on('click', function() {
        		$('.ed-news-items-loader').show();
	           course_id = $(this).attr("data-course-id");
	           $('#ednewsitemscourses').html($(this).text());
	           $('#ednewsitemscourses').attr("data-course-id", course_id);
	           call_get_forums(course_id);
	       });
           // $(".ed-newsitem-ditem").first().trigger('click');

           // Pagination
           $(document).on('click', '.page_ids', function() {
           		$('.ed-news-items-loader').show();
           		var pageid = $(this).data("page_id");
           		// course_id = $("#ednewsitemscourses").data("course-id");
           		call_get_forums(course_id, pageid);
           });
           function call_get_forums(course_id, pageid = 0) {
           		var service_name = 'block_wks_news_aggregator_get_course_forums';
	           	var getCourseForums = Ajax.call([
	               {
	                   methodname: service_name,
	                   args: {
	                        course_id : course_id,
	                        pageid : pageid,
	                   }
	               }
	            ]);
	            getCourseForums[0].done(function (response) {
	            	$('.ed-news-items-loader').hide();
	            	$("#edforums").html(response.content);
	            });
           }
           $(document).ready(function () {
           		$(".ed-newsitem-ditem").first().trigger('click');
           });
        }
    }
});