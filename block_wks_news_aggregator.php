<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the news item block class, based upon block_base.
 *
 * @package    block_wks_news_aggregator
 * @copyright  1999 onwards Martin Dougiamas (http://dougiamas.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/blocks/wks_news_aggregator/lib.php');
/**
 * Class block_wks_news_aggregator
 *
 * @package    block_wks_news_aggregator
 */
class block_wks_news_aggregator extends block_base {
    function init() {
        $this->title = get_string('pluginname', 'block_wks_news_aggregator');
    }

    function get_content() {
        global $CFG, $USER, $PAGE;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if (empty($this->instance)) {
            return $this->content;
        }

        $PAGE->requires->css('/blocks/wks_news_aggregator/style/styles.css');
        $PAGE->requires->js_call_amd('block_wks_news_aggregator/main', 'init');
        // GET USER COURSES
        $courses = enrol_get_users_courses($USER->id, true, '*');
        if (empty($courses)) {
            $this->content = get_string('nocourseenrolled', 'block_wks_news_aggregator');
            return $this->content;
        }
        // Add Courses Dropdown
        $this->content->text .= get_courses_dropdown($courses);
        $this->content->text .= '<span class="ed-news-items-loader loading-icon icon-no-margin hidden"><i class="icon fa fa-circle-o-notch fa-spin fa-fw " title="Loading" aria-label="Loading"></i></span>';
        $this->content->text .= '<div id="edforums"></div>';
        return $this->content;
    }
    /**
     * Locations where block can be displayed.
     *
     * @return array
     */
    public function applicable_formats()
    {
        return array('my' => true);
    }
}
